@isTest
public class TestDataFactory {

    public static Contract createContractWithAccount(){
        
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        
        try{
           insert testAccount;
           System.debug('Insert Account success!');
        }catch(DmlException e){
            System.debug('Exception occured in createContractWithAccount' + e.getMessage());
        }
        
        Contract testContract = new Contract();
        testContract.Name = 'Test Contract';
        testContract.StartDate = Date.newInstance(2011, 11, 11);
        testContract.AccountId = testAccount.Id;
               
        try{
            insert testContract;
            System.debug('Insert testContract successful!');
        }catch(DmlException e){
            System.debug('Exception occured' + e.getMessage());
        }
		return testContract;        
    }
    
    public static List<Contract> createContractsList(){
        List<Account> accountList = new List<Account>();
        
        accountList.add(new Account());
        accountList.add(new Account());
        
        accountList[0].Name = 'Test Account 1';
        accountList[1].Name = 'Test Account 2';
        
        try{
            insert accountList;
            System.debug('Insert accountList successful');
        }catch(DmlException e){
            System.debug('Exception occurred inserting accountList ' + e.getMessage());
        }
        
        List<Contract> contractList = new List<Contract>();
        
        contractList.add(new Contract());
        contractList.add(new Contract());
        
        contractList[0].Name = 'Test Contract 1';
        contractList[0].AccountId = accountList[0].Id;
        contractList[0].StartDate = Date.newInstance(2011, 12, 12);
        contractList[1].Name = 'Test Contract 2';
        contractList[1].AccountId = accountList[1].Id;
        contractList[1].StartDate = Date.newInstance(2000,10,10);
        
        try{
            insert contractList;
            System.debug('insert contractList successful');
        }catch(DmlException e){
            System.debug('Exception occured at createContractsList() ' + e.getMessage());
        }
        return contractList;
    }
    public static Contract_Clause__c createClause(){
        Contract_Clause__c testClause = new Contract_Clause__c();
        testClause.Name = 'Test Clause';
        testClause.Description__c = 'Sample Clause Description';
        DescribeFieldResult fieldValues = Contract_Clause__c.Type__c.getDescribe();
        List<PicklistEntry> availableValues = fieldValues.getPicklistValues();
        testClause.Type__c = availableValues[0].getValue();
        
        try{
            insert testClause;
            System.debug('Name ' + testClause.Name);
            System.debug('Descripton:' + testClause.Description__c);
            System.debug('Type: ' + testClause.Type__c);
            System.debug('Clause id ' + testClause.Id);
        }catch(DmlException e){
            System.debug('Exception occured ' + e.getMessage());
        }
        
        return testClause;
    }
    
    public static ContractClauseAssociation__c createAssociation(){
        Contract_Clause__c clauseObj = createClause();
        Contract contractObj = createContractWithAccount();
        ContractClauseAssociation__c testAssociation = new ContractClauseAssociation__c();
        testAssociation.Name = 'Test Association';
        testAssociation.Contract__c = contractObj.Id;
        testAssociation.Contract_Clause__c = clauseObj.Id;
        
        System.debug('Name ' + testAssociation.Name);
        System.debug('Contract id ' + testAssociation.Contract__c);
        System.debug('Clause id ' + testAssociation.Contract_Clause__c);
        
        try{
            insert testAssociation;
            System.debug('Insert testAssociation succesful!');
            System.debug('Association id ' + testAssociation.Id);
        }catch(DmlException e){
            System.debug('Exception occured ' + e.getMessage());
        }   
        return testAssociation;
    }
}