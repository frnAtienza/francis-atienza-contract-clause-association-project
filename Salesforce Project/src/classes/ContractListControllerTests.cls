@isTest(seeAllData = false)
private class ContractListControllerTests {
    @isTest
    static void saveAssocTest(){       
        ContractListController testController = new ContractListController();
        Test.setCurrentPage(Page.RecordDetail);
        ContractClauseAssociation__c ccAssoc = new ContractClauseAssociation__c();
        ContractClauseAssociation__c obj = TestDataFactory.g
        ccAssoc.Name = ApexPages.currentPage().getParameters().get('assocName');
        ccAssoc.Contract__c = ApexPages.currentPage().getParameters().get('contrId');
        ccAssoc.Contract_Clause__c = ApexPages.currentPage().getParameters().get('clauseId');
        Test.startTest();
        try{
            insert ccAssoc;
        }catch(DmlException e){
            System.debug('Exception Occurred ' + e.getMessage());
        }
        
        System.assertNotEquals(null, ccAssoc);
        testController.saveAssociation();
        Test.stopTest();
     
        
    }
    
    @isTest
    static void saveTest(){
        ContractListController testController = new ContractListController();
        Test.setCurrentPage(Page.RecordDetail);
        ContractClauseAssociation__c ccAssoc = new ContractClauseAssociation__c();
        ccAssoc.Name = ApexPages.currentPage().getParameters().get('assocName');
        ccAssoc.Contract__c = ApexPages.currentPage().getParameters().get('contrId');
        ccAssoc.Contract_Clause__c = ApexPages.currentPage().getParameters().get('clauseId');
        Test.startTest();
        try{
            insert ccAssoc;
        }catch(DmlException e){
            System.debug('Exception Occurred ' + e.getMessage());
        }
        
        System.assertNotEquals(null, ccAssoc);
        testController.saveContract();
        Test.stopTest();
    }
    
    @isTest
    static void openModalWindowTest(){      
        ContractListController testController = new ContractListController();
        
        Test.startTest();
        testController.showCreateModalWindow();
        System.assertEquals(true, testController.displayPopup);
        Test.stopTest();      
    }
    
    @isTest
    static void getContractsTest(){
        ContractListController testController = new ContractListController();
        List<Contract> testContractList = new List<Contract>();
        testContractList = TestDataFactory.createContractsList();
        List<Contract> sampleList = testController.getContracts();
        Test.startTest();
        System.assertNotEquals(0, sampleList.size());
        Test.stopTest();        
    }
    
    @isTest
    static void backButtonTest(){
        
        ContractListController testController = new ContractListController();
        Test.startTest();
        testController.back();
        System.assert(true, testController.previous);
        Test.stopTest();
        
        
    }
    @isTest
    static void closeModalWindowTest(){
        
        ContractListController testController = new ContractListController();
        
        Test.startTest();
        testController.showCreateModalWindow();
        testController.closeModalWindow();
        System.assertEquals(false, testController.displayPopup);
        Test.stopTest();
        
    }
    
    @isTest
    static void getContractTest(){
        Test.setCurrentPage(Page.RecordDetail);
        Contract contractObj = new Contract();
        contractObj = TestDataFactory.createContractWithAccount();
        ContractListController testController = new ContractListController();
        Contract sampleContract = new Contract();
        
        Test.startTest();
        sampleContract = testController.getContract();
        System.debug('sampleContract id: ' + sampleContract.Id);
        System.assertEquals(contractObj.Id, sampleContract.Id);       
        Test.stopTest();
    }
    
    @isTest
    static void deleteClauseTest(){
        ContractClauseAssociation__c assocObj = TestDataFactory.createAssociation();
        ContractListController testController = new ContractListController();
        
        Test.startTest();
          try{
            List<ContractClauseAssociation__c> assocList = testController.getAssociations();
            testController.cca = [SELECT id FROM ContractClauseAssociation__c WHERE id =: assocObj.Id];
            System.debug('Association to be deleted id: ' + testController.cca.Id);
            delete testController.cca;
        }catch(DmlException e){
            System.debug('Exception occured in DeleteClause() ' + e.getMessage());
        }
        testController.DeleteClause();
		Test.stopTest();        
    }
}