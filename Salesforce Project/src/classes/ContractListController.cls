public class ContractListController {
    
    //Getter Setter
    public boolean displayPopup{get;set;}
    public PageReference previous{get;set;}
    public ContractClauseAssociation__c cca{get;set;}
    public Contract contra{get;set;}
    
    //This method creates a new Contract Clause Association record from an open modal window. The modal window is closed upon
    //saving and the page is returned to the current contract detail page.
    public pageReference saveAssociation(){ 
        try{
           upsert cca; 
        }catch(DmlException e){
            System.debug('Exception occured ' + e.getMessage());
        }
        closeModalWindow();
        return null;
    }
    
    public pageReference saveContract(){
        contra.Status = 'Draft';
        try{
            insert contra;
        }catch(DmlException e){
            System.debug('Exception occured: ' + e.getMessage());
        }
        closeModalWindow();
        return null;
    }
    
    //Method for command button action back. Return to landing page
    public pageReference back(){
        
        previous = new PageReference('/apex/ContractLandingPage');
        previous.setRedirect(true);
        return previous;
    }
    //Get a list of contracts for display in the landing page.
    public List<Contract> getContracts(){
        List<Contract> results = [SELECT Name, ContractNumber, AccountId, ContractTerm, StartDate, EndDate, Status 
                                  FROM Contract
                                  ORDER BY Name];
        return results;   
    }
    
    //Get the fields of a specific contract to be used for display in the Contract detail page
    public Contract getContract(){
        
        return [SELECT Id, ContractNumber, AccountId, ContractTerm, Name, StartDate, EndDate 
                FROM Contract 
                WHERE id = :ApexPages.currentPage().getParameters().get('id')];
    }
    
    //Get a list of contract clause associations to display as related list for contract details page.
    public List<ContractClauseAssociation__c> getAssociations(){
        
        return[SELECT Name, Contract_Clause__r.Name, Contract_Clause__r.Description__c, Contract__r.Name, Contract__r.id, Contract_Clause__r.Type__c 
               FROM ContractClauseAssociation__c 
               WHERE Contract__r.id = :ApexPages.currentPage().getParameters().get('id')
               ORDER BY Name ];
    }
    
    //Method called when 'New Contract Clause Association' button is called. Popup a modal window to show form for creating
    //new contract clause association
    public void showCreateModalWindow(){
        	id contractid = ApexPages.currentPage().getParameters().get('id');
        	contra = new Contract();
        	displayPopup = true;
            cca = new ContractClauseAssociation__c();
       		cca.Contract__c = contractid; 			
    }
    
    //Method called when cancel is clicked. Closes the modal window
    public void closeModalWindow(){
        displayPopup = false;
    }
    
    //Method to delete a listed clause in the Contract details page
    public PageReference DeleteClause(){        
        ID cClauseAssociationID = ApexPages.currentPage().getParameters().get('ContractClauseAssociationID');
        cca = [SELECT id FROM ContractClauseAssociation__c WHERE id =: cClauseAssociationID];
        delete cca;
        return null;
    }  
}