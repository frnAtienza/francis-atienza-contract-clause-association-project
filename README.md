This document describes the process a given Salesforce usecase.

Tools used for this usecase:

-IntelliJ IDE

-Sourcetree

-Bitbucket version control

-Force.com Dev Console

While this usecase can be solved using solely Salesforce out-of-box features, this usecase was solved using Lightning designed stylesheets.
The screenshots that demonstrates the look of the custom UI is provided in a separate folder.